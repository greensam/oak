# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# 50.times do |n|
#     Announcement.create(title: "This is test announcement #{n}", content: "test test test", created_by: "Seeded!")
# end

User.create!(name: "Samuel Green",
            pledge_name: "Potatodick",
            grad_year: "2017",
            email: "samuelgreen@college.harvard.edu",
            password: "hello!",
            password_confirmation: "hello!",
            admin: 1,
            activated: true,
            approved: true,
            city: "Cambridge",
            activated_at: Time.zone.now)
            
User.create!(name: "Jacob Bradt",
            pledge_name: "The Tripod Has Fallen",
            grad_year: "2016",
            email: "jtbradt@gmail.com",
            password: "rolltide",
            password: "rolltide",
            admin: 1,
            activated: true,
            approved: true,
            city: "Cambridge",
            activated_at: Time.zone.now)

 30.times do |n|
    User.create(name: "User #{n}", email:"#{n}@example.com", 
                pledge_name: "User #{n}!", 
                grad_year: 1990+n, 
                password:"foobar", 
                password_confirmation:"foobar",
                admin: 0,
                activated: true,
                approved: true,
                city: "#{n} city",
                activated_at: Time.zone.now)
                
end
                
# 10.times do |n|
#     n += 31
#     User.create(name: "User #{n}", email:"#{n}@example.com", 
#                 pledge_name: "User #{n}!", 
#                 grad_year: 1990+n, 
#                 password:"foobar", 
#                 password_confirmation:"foobar",
#                 admin: 0,
#                 activated: true,
#                 approved: nil,
#                 activated_at: Time.zone.now)
# end