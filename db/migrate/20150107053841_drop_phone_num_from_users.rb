class DropPhoneNumFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :phone_num
  end
end
