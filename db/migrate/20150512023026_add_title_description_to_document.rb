class AddTitleDescriptionToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :title, :string
    add_column :documents, :description, :text
  end
end
