class AddCreatedByNameToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :created_by_name, :string
  end
end
