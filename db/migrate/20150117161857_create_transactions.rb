class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.decimal :amount
      t.string :created_by
      t.datetime :confirmed_at
      t.string :confirmed_by

      t.timestamps null: false
    end
  end
end
