class DeleteColumnsFromTransactions < ActiveRecord::Migration
  def change
    remove_column :transactions, :created_by
    remove_column :transactions, :created_by_name
  end
end
