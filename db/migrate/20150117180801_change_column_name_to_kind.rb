class ChangeColumnNameToKind < ActiveRecord::Migration
  def change
    rename_column :transactions, :type, :kind
  end
end
