class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.string :title
      t.text :content
      t.datetime :created_at
      t.string :created_by

      t.timestamps
    end
  end
end
