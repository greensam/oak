require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = User.new(name:"Samuel Green", 
                     pledge_name: "Predging the Oak Crub", 
                     email: "sam@example.com",
                     password: "password",
                     password_confirmation: "password",
                     grad_year: 2005)
  end
  
  test "users should be valid " do
    assert @user.valid?
  end
  
  test "users should have an email" do
    @user.email = " "
    assert_not @user.valid?
  end
  
  test "user emails should not be too long" do
    @user.email = "a" * 256
    assert_not @user.valid? 
  end 
  
  test "users should have a name" do
    @user.name = " "
    assert_not @user.valid?
  end
  
  test "user name should not be too long" do
    @user.name = "a" * 52
    assert_not @user.valid?
  end
  
  test "users should have a pledge name" do
    @user.pledge_name = "  "
    assert_not @user.valid? 
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email addresses should be unique" do
    @duplicate_user = @user.dup
    @user.save
    assert_not @duplicate_user.valid?
  end
  
  test "users must have a graduation year" do
    @user.grad_year = nil
    assert_not @user.valid?
  end
  
end
