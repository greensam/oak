Rails.application.routes.draw do
  
  get 'documents/index'

  get 'documents/show'

  get 'documents/new'

  get 'documents/create'

  get 'documents/edit'

  get 'documents/update'

  get 'dues' => 'transactions#new'
  
  post 'dues' => 'transactions#create'

  get 'payment_history' => 'transactions#index'
  
  get 'user_payment_history' => 'transactions#show'
  
  get 'profile' => 'users#profile'
  
  post 'confirm' => 'transactions#confirm'

  get 'password_resets/new'

  get 'password_resets/edit'

  resources :users
  resources :announcements
  resources :account_activations, only: [:edit, :index]
  resources :transactions
  resources :documents
  
  root "static_pages#home"
  
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get 'logout' => 'sessions#destroy'
  
  get 'sign_up' => 'users#new'
  
  get 'brothers' => 'users#index'
  
  get 'calendar' => 'static_pages#calendar'
  
  post 'approve' => 'account_activations#approve'
  
  resources :password_resets, only: [:new, :create, :edit, :update]
  
  
end
