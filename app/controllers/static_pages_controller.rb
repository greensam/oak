class StaticPagesController < ApplicationController
    before_action :require_login
    
    def calendar
    end
    
    def home
    end
    
end
