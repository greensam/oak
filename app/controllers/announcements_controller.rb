class AnnouncementsController < ApplicationController
    before_action :require_admin, except: [:index, :show]
    before_action :require_login
    
    # landing; lists all of a User's announcements
    def index
        @announcements = Announcement.paginate(:page => params[:page], :per_page => 5).order('created_at DESC')
    end
    
    # create a new announcement for a form
    def new
        @announcement = Announcement.new
    end
    
    # Handle announcement form submission
    def create
        @announcement = Announcement.new(post_params)
        @announcement.created_by = current_user.name
        
        if @announcement.save
            redirect_to announcements_path
        else
            render 'new'
        end
    end
    
    # Retrieve post to show
    def show
        @announcement = Announcement.find(params[:id])
    end
    
    # Setup post editing
    def edit
        @announcement = Announcement.find_by(id: params[:id])
    end
    
    # Update announcement method
    def update
        @announcement = Announcement.find(params[:id])
        
        if @announcement.update(params.require(:announcement).permit(:title, :content))
            redirect_to @announcement
        else
            render 'edit'
        end
    end
    
    # Destroy announcement
    def destroy
        
        @announcement = Announcement.find_by(id: params[:id])
        @announcement.destroy
        
        redirect_to announcements_path
    end
    
    
    private
    
    # ensure that secure submission
    def post_params
        params.require(:announcement).permit(:title, :content)
    end
    
    
end
