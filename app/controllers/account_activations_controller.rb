class AccountActivationsController < ApplicationController
    before_action :require_admin, only: [:index, :approve]
    
    def edit
        user = User.find_by(email: params[:email])
        if user && !user.activated? && user.authenticated?(:activation, params[:id])
            user.activate
            log_in user
            flash[:success] = "Account activated! You will gain full access once an administrator approves your account."
        else
            flash[:danger] = "Invalid activation link"
        end
            redirect_to root_path
    end
    
    def index
        @users = User.where(approved: nil, activated: true)
                        .paginate(:page => params[:page], :per_page => 1).order('activated_at DESC')
    end
    
    def approve
        @user = User.find(params[:id])
        @user.approve
        @user.send_approval_email
        redirect_to account_activations_path
    end
end
