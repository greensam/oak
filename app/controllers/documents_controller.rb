class DocumentsController < ApplicationController
  def index
  end

  def show
  end

  def new
    @doc = Document.new
    puts "Test"
  end

  def create
    @doc = Document.new(document_params)
    if @doc.save
      flash[:info] = "Document Successfully Uploaded"
      redirect_to "index"
    else
      render "new"
    end
  end

  def edit
  end

  def update
  end

  private 
  
  def document_params
    params.require(:doc).permit(:title, :description, :document)
  end

end
