class UsersController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :require_login, only: [:edit, :show, :index]
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
    @user = User.find_by_id(params[:id])
    
    if (!@user || (!current_user.admin && current_user != @user))
      redirect_to root_path
    end
        
  end
  
  def update
    @user = User.find_by_id(params[:id])
    if @user.update_attributes(user_params)
      flash.now[:info] = "Profile updated successfully"
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def destroy
  end
  
  def show
    @user = User.find_by_id(params[:id])
    if !@user 
      redirect_to users_path
    end
  end
  
  def profile
    @user = current_user
    redirect_to @user
  end
  
  def index
    @users = User.where(approved: true).search(params[:search])
                            .order(sort_column + " " + sort_direction)
                            .paginate(:page => params[:page], :per_page => 6)
  end
  
  private 
  
  def user_params
    params.require(:user).permit(:name, :email, :password, 
                                :password_confirmation, 
                                :pledge_name, :grad_year, :job, :phone_number, 
                                :picture, :search, :city)
  end     
  
  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : 'name'
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end
  
  def search_clean
    params[:search].nil? ? params[:search] : nil
  end
  
end
