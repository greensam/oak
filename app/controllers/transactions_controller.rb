class TransactionsController < ApplicationController
  
  before_action :require_login
  before_action :require_admin, only: [:index, :confirm]
  
  def new
    @transaction = Transaction.new
  end

  def create
    @transaction = Transaction.create(transaction_params)
    @transaction.user_id = current_user.id
    @transaction.created_by = current_user.email
    @transaction.email = current_user.email
    @transaction.convert params[:transaction][:amount]
    if @transaction.save
      flash.now[:notice] = "Thank you for your payment, you will receive a confirmation soon."
      redirect_to @transaction.paypal_url(root_url)
    else
      render 'new'
    end
  end

  def confirm
    @transaction = Transaction.find_by(id: params[:id])
    @transaction.confirmed = true
    @transaction.confirmed_by = current_user.name;
    @transaction.confirmed_at = Time.now;
    if @transaction.save
      @transaction.send_confirm_email
    else
      flash.now[:danger] = "Confirmation failed"
    end
    redirect_to payment_history_path
    
  end
  
  def destroy
    @transaction = Transaction.find_by(id: params[:id])
    @transaction.destroy
    
    redirect_to payment_history_path
  end

  def index
    @transactions = Transaction.all.order('confirmed').order('created_at ASC').paginate(page: params[:page], :per_page => 20)
  end

  def show
    @transactions = current_user.transactions.all.paginate(page: params[:page], per_page: 20)
  end
  
  private 
  
  def transaction_params
    params.require(:transaction).permit(:id, :amount, :kind)
  end
  
end
