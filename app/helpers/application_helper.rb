module ApplicationHelper
    
    # helper method for requiring user authentication
    def require_login
        if !logged_in?
            redirect_to login_path
        elsif !current_user.activated?
            flash[:danger] = "Please activate your account!"
            log_out
            redirect_to login_path
        elsif !current_user.approved?
            flash[:danger] = "An administrator has not yet approved your account."
            log_out
            redirect_to login_path
        end
    end
    
    # helper method for requiring admin login
    def require_admin
        if !logged_in?
            redirect_to login_path
        end
        if @current_user.admin < 1
            redirect_to root_path
        end
    end
    
    # helper to return site logo
    def site_logo
        image_tag "oak-shield-red-sq.png", alt: "site logo"
    end
    
    def sortable(column, title = nil)
        title ||= column.titleize
        css_class = (column == sort_column) ? "#{sort_direction}" : nil 
        direction = (column == sort_column && sort_direction == 'asc') ? 'desc' : 'asc'
        link_to title, params.merge(sort: column, direction: direction, page: nil), {:class => css_class}
    end
    
end
