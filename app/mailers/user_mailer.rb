class UserMailer < ApplicationMailer
  default from: "theoakclub@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user

    mail to: user.email, subject: "Oak Online Account Activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user

    mail to: user.email, subject: "Oak Online Password Reset"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_approval.subject
  #
  def account_approval(user)
    @user = user

    mail to: user.email, subject: "Oak Online Account Approved"
  end
  
  
  # Mailer method for confirming user transaction (dues payment, donations, etc.)
  def transaction_confirm(transaction)
    @transaction = transaction

    mail to: transaction.user.email, subject: "Oak: Transaction Confirmed"
  end
  
end
