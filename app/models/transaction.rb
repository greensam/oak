class Transaction < ActiveRecord::Base
    
    belongs_to :user
    
    after_initialize :init
    
    validate :amount_validation
    
    
    def init
        self.confirmed = false if self.confirmed.nil?
    end
    
    def paypal_url(return_url)
        values = {
            business: "theoakclub@gmail.com",
            :cmd => "_cart",
            :upload => 1,
            :return => return_url,
            :invoice => id
        }
        
        values.merge!({
            "amount_1" => self.amount,
            "item_name_1" => self.kind,
            "quantity_1" => 1
        })
        
        "https://www.paypal.com/cgi-bin/webscr?" + values.to_query
    end
    
    def convert(amt)
        if (amt.downcase.include? "new")
            self.amount = 566.25
            self.kind = "New Brother Dues"
        elsif amt.downcase.include? "dues"
            self.amount = 412
            self.kind = "Dues"
        else
            self.amount = nil
            self.kind = nil
        end
    end
    
    def confirm(confirmed_by)
        update_attribute(:confirmed, true)
        update_attribute(:confirmed_by, confirmed_by)
    end
    
    def send_confirm_email
        UserMailer.transaction_confirm(self).deliver_now
    end
    
    private
    
        def amount_validation
            if self.amount == nil
                errors.add(:amount, "should be selected")
            end
        end
    
end
