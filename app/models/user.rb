class User < ActiveRecord::Base
    attr_accessor :remember_token, :activation_token, :reset_token
    
    before_save :downcase_email
    before_create :create_activation_digest
    
    validates :name, presence: true, length: {maximum: 51}
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: {maximum: 255}, 
        format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
    validates :pledge_name, presence: true
    validates :grad_year, presence: true
    validates :city, presence: true
    
    
    has_secure_password
    validates :password, length: {minimum: 6}
    
    mount_uploader :picture, UserImageUploader
    
    validate :picture_size
    
    has_many :transactions
    
    
    def self.search(search)
        if search
            search = search.downcase
            where('name LIKE ? 
                    OR phone_number LIKE ? 
                    OR email LIKE ?
                    OR job LIKE ?
                    OR pledge_name LIKE ?',
                "%#{search}%",
                "%#{search}%",
                "%#{search}%",
                "%#{search}%",
                "%#{search}%")
        else
            where(nil)
        end
    end
    
    # Returns the hash digest of the given string.
    def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
    end
    
    # Returns a random token.
    def User.new_token
        SecureRandom.urlsafe_base64
    end
    
    # Returns true if the given token matches the digest.
    def authenticated?(attribute, token)
        digest = send("#{attribute}_digest")
        return false if digest.nil?
        BCrypt::Password.new(digest).is_password?(token)
    end
    
    def activated?
        self.activated == true
    end
    
    def approved?
        self.approved == true
    end
    
    # called when user activates
    def activate
        update_attribute(:activated, true)
        update_attribute(:activated_at, Time.zone.now)
    end
    
    # sends activation email
    def send_activation_email
        UserMailer.account_activation(self).deliver_now
    end
    
    # called when an admin approves the account
    def approve
        update_attribute(:approved, true)
    end
    
    # sends approval email
    def send_approval_email
        UserMailer.account_approval(self).deliver_now
    end
    
    # Sets the password reset attributes.
    def create_reset_digest
        self.reset_token = User.new_token
        update_attribute(:reset_digest,  User.digest(reset_token))
        update_attribute(:reset_sent_at, Time.zone.now)
    end
    
    # Sends password reset email
    def send_password_reset_email
        UserMailer.password_reset(self).deliver_now
    end
    
    # Returns true if a password reset has expired.
    def password_reset_expired?
        reset_sent_at < 2.hours.ago
    end
    
    private 
    
        def downcase_email
            self.email = email.downcase
        end
    
        # For use in account activation and approval
        def create_activation_digest
            self.activation_token = User.new_token
            self.activation_digest = User.digest(activation_token)
        end
        
        def picture_size 
            if picture.size > 5.megabytes
                errors.add(:picture, "should be less than 5MB")
            end
        end
end
